import socket
import sys

HOST = '127.0.0.1'
PORT = 50007
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST,PORT))
    s.sendall(sys.argv[1].encode())
    data = s.recv(1024).decode()
print('received', repr(data))
