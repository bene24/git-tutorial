##Trasforma un edificio in formato [inizio, fine, altezza] in una heightmap (lista di altezze)
def getHeightmap(building):
	hmap = []
	for i in range(0,building[1]+1):
		if i>=building[0] and i<=building[1]:
			hmap+= [building[2]]
		else:
			hmap+= [0]
	return hmap

#Somma gli elementi di una lista di heightmap e ritorna un'heightmap della skyline
def sumHeightmaps(buildings):
	skyline = []
	max = 0
	for building in buildings:
		if len(building)>max:
			max = len(building)
	for i in range(0,max):
		skyline+=[0]
	for building in buildings:
		for i in range(len(building)):
			if(building[i]>skyline[i]):
				skyline[i]=building[i]
	return skyline

#Trasforma l'heightmap della skyline in una skyline in formato [altezza, indice] registrando solo le variazioni di altezza
def getAltSkyline(skyline):
	newSkyline=[]
	temp=0
	newSkyline+=[[skyline[0],0]]
	for i in range(1,len(skyline)):
		if(skyline[i]!=newSkyline[-1][0]):
			newSkyline+=[[skyline[i],i]]
	return newSkyline





buildings = []
buildings += [getHeightmap([5,10,4])]
buildings += [getHeightmap([4,6,2])]
buildings += [getHeightmap([9,11,6])]
buildings += [getHeightmap([1,2,7])]
buildings += [getHeightmap([3,9,3])]
print(buildings)
print(sumHeightmaps(buildings))
print(getAltSkyline(sumHeightmaps(buildings)))
print("Stelline belle")
print("Fiffafa")
