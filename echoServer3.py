import socket
import signal
import sys

class Server(object):
    def __init__(self, host='', port=80, timeout=5, queue=5):
        self.host = host
        self.port = port
        self.timeout = timeout
        self.buff_size = 1024
        self.listen_queue = queue

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
        signal.signal(signal.SIGINT, self.graceful_shutdown)

    def start(self):
        self.sock.listen(self.listen_queue)
        while True:
            try:
                print("WAITING")
                client, addr = self.sock.accept()
                client.settimeout(self.timeout)
                self._listen_and_repeat(client, addr)
            except Exception as e:
                print(e)
                return

    def _listen_and_repeat(self, client, address):
        while True:
            print("REPEAT")
            try:
                data = client.recv(self.buff_size)
                if not data:
                    break
                request = data.decode().split()
                client.send(self.checkRequest(request))
            except (socket.timeout, ConnectionError) as e:
                print(e)
                break
            client.close()
            return

    def checkRequest(self, request):
        if len(request)<3:
            if request[0] == "GET":
                rfile = ""
                try:
                    file_handler = open(request[1],'rb')
                    rfile = file_handler.read()
                    file_handler.close()
                except Exception as e:
                    rfile = b"<html><body><p>Error: File not found</p></body></hmtl>"
                return rfile
            else:
                return b"<html><body><p>Error: Unsupported method</p></body></hmtl>"
        else:
            return b"<html><body><p>Error: Unsupported format</p></body></hmtl>"

    def graceful_shutdown(self, sig, dummy):
        self.sock.shutdown(socket.SHUT_RDWR)
        self.sock.close()
        sys.exit(1)

if __name__ == "__main__":
    port = 50007
    host = ''
    timeout = 5
    listen_queue = 5

    try:
        s = Server('', port, timeout, listen_queue)
        s.start()
    except PermissionError:
        print("Porta utilizzata")
